use std::io::Write;

use bitvec::{array::BitArray, order::Lsb0};
use itertools::Itertools;

#[macro_use]
extern crate lazy_static;

lazy_static! {
    static ref FACTORS: Vec<Vec<usize>> = {
        let mut f = vec![vec![]];
        for n in 1..=MAX_N {
            let mut ds = vec![];
            for d in 1..=n / 2 {
                if n % d == 0 {
                    ds.push(d);
                }
            }
            f.push(ds);
        }
        f
    };
    static ref CHEQUES_BY_NUM_FACTORS: Vec<usize> = {
        (1..=MAX_N)
            .sorted_by_key(|c| (FACTORS[*c].len(), MAX_N - *c))
            .collect_vec()
    };
    static ref IS_PRIME: Vec<bool> = FACTORS.iter().map(|ds| ds.len() == 1).collect_vec();
    static ref PRIMES: Vec<usize> = {
        IS_PRIME
            .iter()
            .enumerate()
            .filter_map(|(i, is_prime)| if *is_prime { Some(i) } else { None })
            .collect_vec()
    };
}

fn factors(n: usize) -> impl Iterator<Item = usize> {
    FACTORS[n].iter().cloned()
}

const MAX_N: usize = 63;

type Cheques = BitArray<Lsb0, [u64; bitvec::mem::elts::<u64>(MAX_N + 1)]>;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
struct TaxState {
    n: usize,
    cheques: Cheques,
    tax_cheques: Cheques,
    player_sum: usize,
}

impl TaxState {
    fn new(n: usize) -> Self {
        let mut cheques = Cheques::zeroed();
        for c in 1..=n {
            cheques.set(c, true);
        }
        Self {
            n,
            cheques,
            tax_cheques: cheques,
            player_sum: 0,
        }
    }
    fn take(&self, cheque: usize) -> (Self, usize) {
        let mut next = self.clone();
        next.cheques.set(cheque, false);
        next.tax_cheques.set(cheque, false);
        next.player_sum += cheque;
        let mut num_tax_cheques = 0;
        for d in factors(cheque) {
            if next.tax_cheques[d] {
                num_tax_cheques += 1;
            }
            next.cheques.set(d, false);
            next.tax_cheques.set(d, false);
        }
        if IS_PRIME[cheque] {
            for p in PRIMES.iter().cloned() {
                next.cheques.set(p, false);
            }
        }
        (next, num_tax_cheques)
    }
    fn moves(&self) -> Vec<(usize, usize)> {
        let mut moves = vec![];
        for c in 1..=self.n {
            if !self.cheques[c] {
                continue;
            }
            let mut num_tax_cheques = 0;
            for d in factors(c) {
                if self.tax_cheques[d] {
                    num_tax_cheques += 1;
                }
            }
            if num_tax_cheques > 0 {
                moves.push((self.n - num_tax_cheques, c));
            }
        }
        moves.sort();
        moves.reverse();
        moves
    }
    fn upper_bound(&self) -> usize {
        let mut bound = self.player_sum;
        let mut num_cheques = self.tax_cheques.count_ones() / 2;
        for i in (1..=self.n).rev() {
            if num_cheques == 0 {
                break;
            }
            if self.cheques[i] {
                num_cheques -= 1;
                bound += i;
            }
        }
        bound
    }
}

fn noisy_max(a: usize, b: usize) -> usize {
    if b > a {
        print!(" {}\r", b);
        std::io::stdout().flush().unwrap();
        return b;
    }
    a
}

// fn bb(start: TaxState, initial_bound: usize) -> (usize, Vec<(usize, usize)>) {
//     // let mut queue = BinaryHeap::new();
//     let mut queue = Vec::new();
//     queue.push((start.upper_bound(), start));
//     let mut lower_bound = initial_bound;
//     let mut winning_sol = None;

//     while let Some((s_ub, s)) = queue.pop() {
//         if s_ub <= lower_bound {
//             continue;
//         }
//         let cs = s.moves();
//         if cs.len() == 0 {
//             if s.player_sum > lower_bound {
//                 winning_sol = Some(s.hist.clone());
//             }
//             lower_bound = noisy_max(lower_bound, s.player_sum);
//         } else {
//             for (_n_minus_num_tax, c) in cs {
//                 let next = s.take(c);
//                 let ub = next.upper_bound();
//                 if ub > lower_bound {
//                     queue.push((ub, next));
//                 }
//             }
//         }
//     }

//     (lower_bound, winning_sol.unwrap())
// }

fn dfs(
    state: TaxState,
    mut bound: usize,
    path: &mut Vec<(usize, usize)>,
    sol_path: &mut Vec<(usize, usize)>,
) -> usize {
    if state.upper_bound() <= bound {
        return bound;
    }

    let cs = state.moves();
    if cs.len() == 0 {
        if state.player_sum > bound {
            sol_path.splice(.., path.iter().cloned());
        }
        bound = noisy_max(bound, state.player_sum);
    } else {
        for (_n_minus_num_tax, c) in cs {
            let (next, num_tax) = state.take(c);
            path.push((c, num_tax));
            bound = noisy_max(bound, dfs(next, bound, path, sol_path));
            path.pop();
        }
    }
    bound
}

fn solve(n: usize, bound: usize) -> (usize, Vec<(usize, usize)>) {
    let mut path = vec![];
    let mut sol_path = vec![];
    let money = dfs(TaxState::new(n), bound, &mut path, &mut sol_path);
    (money, sol_path)
}

fn main() {
    let mut prev_bound = 0;
    let mut best_bound = (0, 1);
    for i in 2..=MAX_N {
        let total_money = i * (i + 1) / 2;
        let bound = total_money * best_bound.0 / best_bound.1;
        let (max_money, sol) = solve(i, bound);
        if max_money > bound {
            best_bound = (max_money, total_money);
        }
        println!(
            "{:2}: {:.4} ({:3}  {})",
            i,
            max_money as f64 / total_money as f64,
            max_money,
            sol.iter().map(|(m, t)| format!("{:2}({})", m, t)).join(" "),
        );
        prev_bound = max_money;
    }
}
